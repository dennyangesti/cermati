import React, { Component } from 'react'

import { ReactComponent as Calendar } from 'Assets/Icons/calendar.svg'
export default class Challenge extends Component {
	render() {
		const { detailChallenge } = this.props
		return (
			<div className='Register__Challenge'>
				<img src={detailChallenge.image} alt={detailChallenge.name} />
				<div className='Register__Challenge__Date'>
					<Calendar />
					<div className='Register__Challenge__Date--title'>
						{detailChallenge.startDate} - {detailChallenge.endDate}
					</div>
				</div>
			</div>
		)
	}
}
