import React, { Component } from 'react'

import { ReactComponent as Back } from 'Assets/Icons/back.svg'
export default class Nav extends Component {
	render() {
		const { title } = this.props
		return (
			<div className='Challenge__Nav'>
				<Back />
				<div className='Challenge__Nav--title'>{title}</div>
			</div>
		)
	}
}
