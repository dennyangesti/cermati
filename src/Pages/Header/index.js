import React, { Component } from 'react'

import Logo from '../../Assets/Logos/y-logo-white.png'

export default class index extends Component {
	render() {
		const name = 'Denny Angesti Pratama'
		return (
			<div className='Header'>
				<img src={Logo} alt='Y-Logo' />
				<div className='Header__Content'>
					<div className='Header__Content--title'>Hello! I'm {name}</div>
					<div className='Header__Content--subtitle'>Consult, Design, and Develop Websites</div>
					<div className='Header__Content--desc'>
						Have something great in mind? Feel free to contact me.
						<br />
						I'll help you to make it happen
					</div>
					<div className='Header__Content__Button'>
						<button>Let's Make Contact</button>
					</div>
				</div>
			</div>
		)
	}
}
