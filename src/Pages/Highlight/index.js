import React, { Component } from 'react'

import { ReactComponent as Brush } from '../../Assets/Icons/brush.svg'
import { ReactComponent as Chart } from '../../Assets/Icons/chart.svg'
import { ReactComponent as Chat } from '../../Assets/Icons/chat.svg'
import { ReactComponent as Cube } from '../../Assets/Icons/cube.svg'
import { ReactComponent as Graph } from '../../Assets/Icons/graph.svg'
import { ReactComponent as Shout } from '../../Assets/Icons/shout.svg'

export default class index extends Component {
	constructor(props) {
		super(props)

		this.state = {
			cards: [
				{
					id: 1,
					title: 'Consult',
					icon: Chat,
					text:
						'Co-create, design thinking; strengthen infrastructure resist granular. Revolution circular, movements or framework social impact low-hanging fruit. Save the world compelling revolutionary progress.',
				},
				{
					id: 2,
					title: 'Design',
					icon: Brush,
					text:
						'Policymaker collaborates collective impact humanitarian shared value vocabulary inspire issue outcomes agile. Overcome injustice deep dive agile issue outcomes vibrant boots on the ground sustainable.',
				},
				{
					id: 3,
					title: 'Develop',
					icon: Cube,
					text:
						'Revolutionary circular, movements a or impact framework social impact low-hanging. Save the compelling revolutionary inspire progress. Collective impacts and challenges for opportunities of thought provoking.',
				},
				{
					id: 4,
					title: 'Marketing',
					icon: Shout,
					text:
						'Peaceful; vibrant paradigm, collaborative cities. Shared vocabulary agile, replicable, effective altruism youth. Mobilize commitment to overcome injustice resilient, uplift social transparent effective.',
				},
				{
					id: 5,
					title: 'Manage',
					icon: Graph,
					text:
						'Change-makers innovation or shared unit of analysis. Overcome injustice outcomes strategize vibrant boots on the ground sustainable. Optimism, effective altruism invest optimism corporate social.',
				},
				{
					id: 6,
					title: 'Evolve',
					icon: Chart,
					text:
						'Activate catalyze and impact contextualize humanitarian. Unit of analysis overcome injustice storytelling altruism. Thought leadership mass incarceration. Outcomes big data, fairness, social game-changer.',
				},
			],
		}
	}

	render() {
		const { cards } = this.state
		return (
			<div className='Highlight'>
				<div className='Highlight--title'>How Can I Help You?</div>
				<div className='Highlight--subtitle'>
					Our work then targeted, best practices outcomes social innovation synergy. Venture philanthropy,
					revolutionary inclusive policymaker relief. User-centered program areas scale.
				</div>
				<div className='Highlight__Wrapper'>
					{cards.map((card) => {
						return (
							<div className='Highlight__Wrapper__Cards' key={card.id}>
								<div className='Highlight__Wrapper__Cards__Header'>
									<div className='Highlight__Wrapper__Cards__Header--title'>{card.title}</div>
									<card.icon />
								</div>
								<div className='Highlight__Wrapper__Cards--text'>{card.text}</div>
							</div>
						)
					})}
				</div>
			</div>
		)
	}
}
