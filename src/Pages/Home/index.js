import React, { Component } from 'react'

import { Header, Highlight, Footer } from '../index'

import { ReactComponent as Close } from '../../Assets/Icons/close.svg'
export default class index extends Component {
	constructor(props) {
		super(props)
		this.state = {
			animationNewsletter: true,
			animationNotification: false
		}
	}

	renderNotification = () => {
		const { animationNotification } = this.state
		return (
			<div className={`Notif ${animationNotification ? 'slideOut' : ''}`}>
				<div className='Notif__Wrapper'>
					<div className='Notif--text'>
						By accessing and using this website, you acknowledge that you have read and understand our{' '}
						<a href='#'>Cookie Policy</a>, <a href='#'>Privacy Policy</a>, and our <a href='#'>Terms of Service</a>.
				</div>
					<div className='Notif__Button'>
						<button
							onAnimationEnd={() => this.setState({ animationNotification: false })}
							onClick={() => this.setState({ animationNotification: true })}
						>Got it
						</button>
					</div>
				</div>
			</div>
		)
	}
	renderNewsletter = () => {
		const { animationNewsletter } = this.state
		return (
			<div className={`News ${animationNewsletter ? 'slideIn' : 'slideOutII'}`}>
				<div className="News--title">Get latest updates in web technologies</div>
				<div className="News--text">I write articles related to web technologies, such as design trends, development
				tools, UI/UX case studies and reviews, and more. Sign up to my newsletter to get
				them all.</div>
				<form className="News__Form" action="">
					<input type="email" placeholder='Email Address' />
					<button>Count me in!</button>
				</form>
				<Close onClick={() => this.setState({ animationNewsletter: false })} />
			</div>
		)
	}
	render() {
		return (
			<div className='Home'>
				{this.renderNotification()}
				<Header />
				<Highlight />
				<Footer />
				{this.renderNewsletter()}
			</div>
		)
	}
}
